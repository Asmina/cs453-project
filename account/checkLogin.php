<?php
session_start();
$alert = '';
try
{
  $pdo = new PDO('mysql:host=localhost;dbname=tMOM', 'jxb', 'Gh0stT0ast');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e)
{
  $error = 'Unable to connect to the database server.';
  include 'error.html.php';
  exit();
}

$myusername=$_POST["userlogin"];
$userpwd=$_POST["userpwd"];
$loginExists = false;
$pwdCorrect = false;

try
{
    $sql = "SELECT * FROM t_user
    WHERE userName = '$myusername'";
    $result = $pdo->query($sql);
}
catch (PDOException $e)
{
    $error = 'Error checking user login: ' . $e->getMessage();
    include 'error.html.php';
    exit();
}

//iterate through usernames in db. If match, check password
foreach ($result as $row) {
    if ($myusername == $row['userName']) {
        $loginExists = true;
        if (password_verify($userpwd, $row['pwdHash'])) {
            $pwdCorrect = true;
        }
        break;
    }       
}

if (!$loginExists) {
    $alert = 'Sorry, this username does not exist. Please create a new account.';
}
else if (!$pwdCorrect) {
    $alert = 'Incorrect password. Please try again.';
}
else {
    $alert = 'Welcome to the site, '.$myusername.'!';
    $_SESSION["username"] = $myusername;
    header("location: ../index.php");
}

include 'login.html';
