<?php
session_start();
session_unset('username');
session_unset('isEmployee');
$alert = "You have successfully logged out.";

header("location: ../index.php");
exit();
?>