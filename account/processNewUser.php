<?php
try
{

  $pdo = new PDO('mysql:host=localhost;dbname=tMOM', 'jxb', 'Gh0stT0ast');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e)
{
  $error = 'Unable to connect to the database server.';
  include 'error.html.php';
  exit();
}

$newlogin = $_POST['newlogin'];
$newpwd = $_POST['newpwd'];
$newEmail = $_POST['newEmail'];
$empID = $_POST['empID'];

$newPwdHash = password_hash($newpwd,PASSWORD_DEFAULT);

// try
// {
//   $sql = "SELECT COUNT(userName)
//           FROM t_user
//           WHERE userName = :newlogin";
//   $s = pdo->prepare($sql);
//   $s->bindValue(':newlogin', $newlogin);

//   $result = $s->execute();
//   $rows = mysql_num_rows($result);
// }
// catch
// {
//   $error = 'Error creating new account: ' . $e->getMessage();
//     include 'error.html.php';
//     exit();
// }

// if ($rows > 0) {
//   $alert = "Sorry. That username already exists.";
//   include 'register.html';
//   exit();
// }

//Query the employee ID if entered by the user...
if ($empID == '') {
  $isEmployee = 0;
}
else 
{
  try
  {
    $sql = "SELECT * FROM t_employee WHERE employeeID = '$empID'";

      $result = $pdo->query($sql);
  }
    catch (PDOException $e)
  {
      $error = 'Error creating new account: ' . $e->getMessage();
  }

  //...then check if the employee ID exists in the DB.
  if ($result->rowCount() < 1)
  {
    $alert = "That is not a valid employee ID";
    include 'register.html';
    exit();
  }
  else {
    $isEmployee = 1;
  }
}

try
{
	$sql = 'INSERT INTO t_user SET
        userName = :newlogin,
        pwdHash = :newPwdHash,
        userEmail = :newEmail,
        isEmployee = :isEmployee';

    $s = $pdo->prepare($sql);
    $s->bindValue(':newlogin', $newlogin);
    $s->bindValue(':newPwdHash', $newPwdHash);
    $s->bindValue(':newEmail', $newEmail);
    $s->bindValue(':isEmployee', $isEmployee);

    $s->execute();
}
  catch (PDOException $e)
{
    $error = 'Error creating new account: ' . $e->getMessage();
    $alert = "Sorry, that username already exists.";
    include 'register.html';
    exit();
}
$alert = "Account successfully created! Please login below.";

include 'login.html';
exit();
?>