<?php 

session_start();

//If a user is not logged in, take the user to the login form.
if(!isset($_SESSION['username'])) {
	header("location: account/login.html");
	exit();
} //end if statement
//Otherwise, grant access to the rest of the website with the user's credentials

//If we don't know if the user is an employee yet, connect to db and find out.
//Then direct them to the appropriate web pages.
if (!isset($_SESSION['isEmployee'])) {
	try 
	{
	  $pdo = new PDO('mysql:host=localhost;dbname=tMOM', 'jxb', 'Gh0stT0ast');
	  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  $pdo->exec('SET NAMES "utf8"');
	}
	catch (PDOException $e)
	{
	  $error = 'Unable to connect to the database server.';
	  include 'error.html.php';
	  exit();
	}

	$username = $_SESSION['username'];
	$sql = "SELECT * FROM t_user WHERE userName = '$username'";
	$result = $pdo->query($sql);
	$row = $result->fetch();
	if ($row['isEmployee'] == 1) {
		include ('masterEmployee.html');
		$_SESSION['isEmployee'] = true;
		exit();
	}
	else {
		$_SESSION['isEmployee'] = false;
		include 'master.html';
		exit();
	}

	if ($_SESSION['isEmployee']) {
		header("location: masterEmployee.html");
	}
	if (!$_SESSION['isEmployee']) {
		header("location: master.html");
	}
}