create table t_employee (
employeeID  int          not null,
firstName   varchar(15)  not null,
lastName    varchar(15)  not null, 
empPhone    int          not null,
CONSTRAINT employee_PK PRIMARY KEY (employeeID)
) ;
create table t_user(
    
userName    varchar(15) not null, 
pwdHash     varchar(255) not null, 
userEmail   varchar(15) not null,
isEmployee  boolean(1) not null, 
CONSTRAINT user_PK PRIMARY KEY (userName) 
);
create table t_client (
clientPhone  varchar(10)  not null,
clientName   varchar(15)  not null,
clientEmail  varchar(15), 
CONSTRAINT Client_PK PRIMARY KEY (clientPhone)
);
create table t_suite (
floorNumber int   not null,
suiteNumber int   not null,
CONSTRAINT Suite_PK PRIMARY KEY (floorNumber,suiteNumber)
);
create table t_tenant (
tenantID        int           not null,
tenantName      varchar(15)   not null,
tenantPhone     int(10)       not null,
tenantEmail     varchar(15),
tenantWebsite   varchar(15),
CONSTRAINT Tenant_PK PRIMARY KEY (tenantID)
);
create table t_category (
categoryCode   int           not null,
categoryName   varchar(15)   not null,
CONSTRAINT Category_PK PRIMARY KEY (categoryCode)
);
create TABLE t_eventSpace (
spaceName      varchar(20)    not null,
floorNumber    int    not null,
approxCap      int,
CONSTRAINT EventSpace_PK PRIMARY KEY (spaceName)
);
create table t_isUnder (
categoryCode    int   not null,
tenantID        int   not null,
CONSTRAINT IsUnder_PK PRIMARY KEY (categoryCode,tenantID)
);
create table t_worksAt (
eventID   int   not null,
employeeID     int   not null,
CONSTRAINT WorksAt_PK PRIMARY KEY (eventID, employeeID)
);
create table t_maintenance (
maintenanceID    int                not null,
skills            varchar(15),
CONSTRAINT Maintenance_PK PRIMARY KEY (maintenanceID)
);
create table t_skills (
skillName varchar(15) not null,
CONSTRAINT Skills_PK PRIMARY KEY (skillName)
);
create table t_event (
eventID       int           not null,
spaceName     varchar(15)   not null,
clientPhone   varchar(10)      not null,
startDate     DATE,
startTime     TIME,
endDate       DATE,
endTime       TIME,
CONSTRAINT Event_PK PRIMARY KEY (eventID),
CONSTRAINT EventEventSpace_FK FOREIGN KEY (spaceName)REFERENCES t_eventSpace (spaceName),
CONSTRAINT EventClient_FK FOREIGN KEY (clientPhone) REFERENCES t_client (clientPhone)
);
create table t_host (
workPhone       bigint    not null,
employeeID      int    not null,
CONSTRAINT Host_FK FOREIGN KEY (employeeID) REFERENCES t_employee (employeeID)
);
ALTER table t_suite add COLUMN area bigint;
ALTER table t_suite add COLUMN tenantID int;
UPDATE t_suite
Set tenantID = 2134 
WHERE suiteNumber = 11 AND floorNumber = 3;
UPDATE t_suite
Set tenantID = 2278 
WHERE suiteNumber = 2 AND floorNumber = 2;
UPDATE t_suite
Set tenantID = 2563 
WHERE suiteNumber = 12 AND floorNumber = 12;
UPDATE t_suite
Set tenantID = 2176 
WHERE suiteNumber = 1 AND floorNumber = 7;
UPDATE t_suite 
Set tenantID = 2173 
WHERE suiteNumber = 16 AND floorNumber = 7;
alter table t_maintenance
add COLUMN employeeID int not null;
UPDATE t_maintenance 
set employeeID = 2595
WHERE maintenanceID = 1111;
UPDATE t_maintenance 
set employeeID = 3469
WHERE maintenanceID = 1130;
UPDATE t_maintenance 
set employeeID = 2378
WHERE maintenanceID = 1145;
UPDATE t_maintenance 
set employeeID = 2690
WHERE maintenanceID = 1196;
UPDATE t_maintenance 
set employeeID = 6366
WHERE maintenanceID = 1167;
alter table t_host add 
COLUMN eventID int not null;